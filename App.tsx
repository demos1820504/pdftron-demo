import React from "react";
import { SafeAreaView, StyleSheet, useWindowDimensions } from "react-native";
import { RNPdftron, Config, DocumentView } from "@pdftron/react-native-pdf";

RNPdftron.initialize("");

export default function App() {
  const { height, width } = useWindowDimensions();
  return (
    <SafeAreaView style={styles.container}>
      <DocumentView
        style={{ height: height - 100, width }}
        readOnly
        annotationToolbars={[Config.DefaultToolbars.View]}
        hideAnnotationToolbarSwitcher
        showNavigationListAsSidePanelOnLargeDevices
        thumbnailViewEditingEnabled={false}
        hideThumbnailFilterModes={[
          Config.ThumbnailFilterMode.Annotated,
          Config.ThumbnailFilterMode.Bookmarked,
        ]}
        bottomToolbar={[
          Config.Buttons.thumbnailsButton,
          Config.Buttons.reflowButton,
        ]}
        hideViewModeItems={[Config.ViewModePickerItem.Crop]}
        pageStackEnabled={false}
        showLeadingNavButton={false}
        topAppNavBarRightBar={[
          Config.Buttons.searchButton,
          Config.Buttons.viewControlsButton,
        ]}
        overrideBehavior={[Config.Actions.linkPress]}
        document="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
